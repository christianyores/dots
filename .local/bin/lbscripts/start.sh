#!/bin/bash
. "${HOME}/.cache/wal/colors.sh"

(~/.local/bin/lbscripts/feed.sh | lemonbar -p -B "#aa${color0:1}" -F "${color15}" -n "bspwm" -g 1920x20 -f "LiberationMono:size=12:antialias=true:autohint=true" -f "NotoColorEmoji:size=10") &
