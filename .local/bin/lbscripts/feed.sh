#!/usr/bin/bash

Clock() {
	TIME=$(date "+%a %b %d %H:%M")
	echo -e -n "${TIME}"
}

Volume() {
    VOL=$(amixer sget Master | grep -o "\[.*\%\]")
    echo -e -n "🔉${VOL:1:3}"
}

Battery() {
    BAT0=$(cat /sys/class/power_supply/BAT0/capacity)
    BAT1=$(cat /sys/class/power_supply/BAT1/capacity)
    AVG=$((BAT0+BAT1))
    AVG=$((AVG/2))
    echo -e -n "🔋${AVG}%"
}

while true; do
    echo -e "%{l} $(~/.local/bin/lbscripts/bspwm.sh)" "%{c}$(Clock)" "%{r} $(Volume) $(Battery)"
	sleep 1
done
